# Blidness-APTOS

### Augmentation types:

Code used for Kaggle [blindness-detection competition](https://www.kaggle.com/c/aptos2019-blindness-detection).

Possible data augmentation:
- rotation
- horizontal/vertical flip
- color (some photos are partially green)
- brightness (check how to change by 20% looks like on train set) (brightness is not an indicator of a class)
- zoom in min : 10% max 20% (but strongly depends on the picture so maybe can be improved e.g. probability of a cut is based on the number of black pixels in the input image)
- zoom out + crop (same as zoom in)

- bloody patches: Strong indicator (c3/4)
- symmetrical circles with mold-like texture mostly on the edges: Strong indicator (c3/4)

- different color mist like glow (green on the edges): Not an indicator
- shininess: Not an indicator
- a lot of blood vessels illuminated form the other side: Not an indicator
- brightness: Not an indicator